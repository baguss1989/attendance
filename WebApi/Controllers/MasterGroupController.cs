﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Web.Mvc;
using WebApi.DAL;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class MasterGroupController : ApiController
    {
        private ATT_MASTER_GROUP_DAL groupDal = new ATT_MASTER_GROUP_DAL();

        // GET: api/MasterGroup
        public IEnumerable<ATT_MASTER_GROUP> Get()
        {
            var result = groupDal.GetAll();
            return result;
        }

        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/MasterGroup/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/MasterGroup
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/MasterGroup/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/MasterGroup/5
        public void Delete(int id)
        {
        }
    }
}
