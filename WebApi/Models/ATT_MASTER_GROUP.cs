﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class ATT_MASTER_GROUP
    {
        public string GROUP_ID { get; set; }
        public string GROUP_CODE { get; set; }
        public string GROUP_DESC { get; set; }
        public string GROUP_STATUS { get; set; }
    }
}